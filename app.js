var express = require('express');
var path = require('path');
var fs = require('fs');

var app = express();
var multer  = require('multer')
var upload = multer({
  dest: 'uploads/'
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.get('/', function (req, res) {
  res.render('index');
});

app.post('/', upload.single('upload'), function (req, res) {
  res.render('index', {size: req.file.size});
  fs.unlink(req.file.path);
  // res.json({size:req.file.size});
});

module.exports = app;